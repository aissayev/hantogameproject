/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.epsilon;

import static hanto.common.HantoPieceType.*;
import static hanto.common.MoveResult.*;
import static hanto.common.HantoPlayerColor.*;
import static org.junit.Assert.*;
import hanto.common.*;
import hanto.studentaissayev.common.HantoGameFactory;

import org.junit.*;

/**
 * Test cases for Gamma Hanto.
 * @author aissayev
 *
 */
public class EpsilonHantoMasterTest {
	
	/**
	 * Internal class for these test cases
	 */
	public class TestHantoCoordinate implements HantoCoordinate
	{
		private final int x,y;
		
		/**
		 * Constructor for test Hanto Coordinate
		 * @param x
		 * @param y
		 */
		public TestHantoCoordinate(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
		/*
		 * @see hanto.common.HantoCoordinate#getX()
		 */
		@Override
		public int getX()
		{
			return x;
		}

		/*
		 * @see hanto.common.HantoCoordinate#getY()
		 */
		@Override
		public int getY()
		{
			return y;
		}		
	}

	private static HantoGameFactory factory = HantoGameFactory.getInstance();
	private HantoGame game;
	
	/**
	 * Initialize the game before testing
	 */
	@BeforeClass
	public static void initializeClass()
	{
		factory = HantoGameFactory.getInstance();
	}
	
	/**
	 * game setup
	 */
	@Before
	public void setup()
	{
		game = factory.makeHantoGame(HantoGameID.EPSILON_HANTO, BLUE);
	}
	
	/**
	 * blue places butterfly first
	 * @throws HantoException
	 */
	@Test	// 1
	public void bluePlacesButterflyFirst() throws HantoException
	{
		final MoveResult mr = game.makeMove(BUTTERFLY, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
		checkPieceAt(0, 0, BLUE, BUTTERFLY);
	}
	
	/**
	 * red tries to place sparrow first
	 * @throws HantoException
	 */
	@Test	// 2
	public void redPlacesSparrowFirst() throws HantoException
	{
		game = factory.makeHantoGame(HantoGameID.EPSILON_HANTO, RED);
		final MoveResult mr = game.makeMove(SPARROW, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
	}
	
	/**
	 * blue places crab first
	 * @throws HantoException
	 */
	@Test	// 3
	public void bluePlacesCrabFirst() throws HantoException
	{
		final MoveResult mr = game.makeMove(CRAB, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
		checkPieceAt(0, 0, BLUE, CRAB);
	}
	
	/**
	 * blue places horse first
	 * @throws HantoException
	 */
	@Test	// 4
	public void bluePlacesHorseFirst() throws HantoException
	{
		final MoveResult mr = game.makeMove(HORSE, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
		checkPieceAt(0, 0, BLUE, HORSE);
	}
	
	/**
	 * Crab Walks
	 * @throws HantoException
	 */
	@Test	// 5
	public void blueCrabWalksOneHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(-1,2)));	
	}
	
	/**
	 * Crab can not walks two hexes or more in Epsilon
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 6
	public void crabWalksTwoHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(-1,1)));	
	}
	
	/**
	 * Make red sparrow fly less than max
	 * @throws HantoException
	 */
	@Test	// 7
	public void redFlySparrow() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(CRAB, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(SPARROW, null, makeCoordinate(1,1));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,2));
		assertEquals(OK, game.makeMove(SPARROW, makeCoordinate(1,-1), makeCoordinate(-1,1)));
		
	}
	
	/**
	 * Make red sparrow fly  max
	 * @throws HantoException
	 */
	@Test	// 8
	public void blueFlySparrowMax() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(SPARROW, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(1,2));
		assertEquals(OK, game.makeMove(SPARROW, makeCoordinate(0,-1), makeCoordinate(2,2)));
		
	}
	
	/**
	 * Make red sparrow fly more than max
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 9
	public void blueFlySparrowMoreThanMax() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(SPARROW, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(-2,0));
		game.makeMove(CRAB, null, makeCoordinate(2,2));
		assertEquals(OK, game.makeMove(SPARROW, makeCoordinate(0,-1), makeCoordinate(3,2)));
		
	}
	
	/**
	 * blue horse jumps
	 * @throws HantoException
	 */
	@Test	// 10
	public void blueMovesHorse() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(0,-1));
		game.makeMove(HORSE, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(-2,0));
		game.makeMove(CRAB, null, makeCoordinate(2,2));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(0,-1), makeCoordinate(0,3)));
	}
	
	/**
	 * blue tries horse jumps in not linear
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 11
	public void blueMovesHorseNonLinear() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(0,-1));
		game.makeMove(HORSE, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(-2,0));
		game.makeMove(CRAB, null, makeCoordinate(2,2));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(0,-1), makeCoordinate(1,0)));
	}
	
	/**
	 * blue tries invalid horse jump (no pieces in between)
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 12
	public void blueMovesHorseNoPiecesInBetween() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(0,-1));
		game.makeMove(HORSE, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(-2,0));
		game.makeMove(CRAB, null, makeCoordinate(1,3));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(0,-1), makeCoordinate(0,4)));
	}
	
	/**
	 * red make disconnecting horse jump 
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 13
	public void redMovesHorseDisconnects() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(0,-1));
		game.makeMove(HORSE, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(-2,0));
		game.makeMove(CRAB, null, makeCoordinate(2,2));
		game.makeMove(CRAB, null, makeCoordinate(2,-2));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(1,2), makeCoordinate(-2,2)));
	}
	
	/**
	 * blue tries jump trapped horse
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 14
	public void blueMovesHorseTrap() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(0,-1));
		game.makeMove(HORSE, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(0,-2));
		game.makeMove(CRAB, null, makeCoordinate(2,2));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(0,-1), makeCoordinate(0,3)));
	}
	
	/**
	 * blue wins with horse move
	 * @throws HantoException
	 */
	@Test	// 15
	public void blueWinsWithHorseMove() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(1,-2));
		game.makeMove(HORSE, null, makeCoordinate(-1,1));
		game.makeMove(HORSE, null, makeCoordinate(-2,1));
		game.makeMove(CRAB, null, makeCoordinate(1,0));
		assertEquals(BLUE_WINS, game.makeMove(HORSE, makeCoordinate(-2,1), makeCoordinate(1,1)));
	}
	
	/**
	 * red wins with crab move
	 * @throws HantoException
	 */
	@Test	// 16
	public void redWinsWithCrabMove() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(CRAB, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(0,-1));
		game.makeMove(HORSE, null, makeCoordinate(1,0));
		game.makeMove(HORSE, null, makeCoordinate(-2,1));
		assertEquals(RED_WINS, game.makeMove(CRAB, makeCoordinate(-1,2), makeCoordinate(-1,1)));
	}
	
	/**
	 * red moves wrong piece
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 16
	public void redMovesWrongPiece() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(0,2), makeCoordinate(-1,2)));
	}
	
	/**
	 * blue moves horse to occupied hex
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 17
	public void blueMovesHorseToOccupiedHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(1,-2));
		game.makeMove(HORSE, null, makeCoordinate(-1,1));
		game.makeMove(HORSE, null, makeCoordinate(-2,1));
		game.makeMove(CRAB, null, makeCoordinate(1,0));
		assertEquals(BLUE_WINS, game.makeMove(HORSE, makeCoordinate(-2,1), makeCoordinate(0,1)));
	}
	
	/**
	 * red moves horse to next hex
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 18
	public void redMovesHorseMoveSizeOne() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(HORSE, null, makeCoordinate(0,2));
		game.makeMove(HORSE, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(HORSE, makeCoordinate(0,2), makeCoordinate(-1,2)));
	}
	
	/**
	 * blue flies sparrow disconnecting the group
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 19
	public void blueMovesSparrowDisconnects() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,2));
		game.makeMove(HORSE, null, makeCoordinate(1,-2));
		game.makeMove(HORSE, null, makeCoordinate(1,2));
		game.makeMove(CRAB, null, makeCoordinate(-2,0));
		game.makeMove(CRAB, null, makeCoordinate(2,2));
		assertEquals(OK, game.makeMove(SPARROW, makeCoordinate(1,-1), makeCoordinate(1,1)));
	}
	
	/**
	 * red resigns while there are still available moves
	 * @throws HantoException
	 */
	@Test(expected = HantoPrematureResignationException.class)	// 20
	public void redResignsWithAvailableMoves() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,1));
		game.makeMove(CRAB, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(null, null, null);
		assertTrue(true); // code audit
	}
	
	
	/**
	 * Make sure that the piece at the location is what you expect
	 * @param x x-coordinate
	 * @param y y-coordinate
	 * @param color piece color expected
	 * @param type piece type expected
	 */
	private void checkPieceAt(int x, int y, HantoPlayerColor color, HantoPieceType type)
	{
		final HantoPiece piece = game.getPieceAt(makeCoordinate(x, y));
		assertEquals(color, piece.getColor());
		assertEquals(type, piece.getType());
	}
	

	// Helper methods
	private HantoCoordinate makeCoordinate(int x, int y)
	{
		return new TestHantoCoordinate(x, y);
	}
}
