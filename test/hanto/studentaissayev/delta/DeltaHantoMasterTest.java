/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.delta;

import static hanto.common.HantoPieceType.*;
import static hanto.common.MoveResult.*;
import static hanto.common.HantoPlayerColor.*;
import static org.junit.Assert.*;
import hanto.common.*;
import hanto.studentaissayev.common.HantoGameFactory;

import org.junit.*;

/**
 * Test cases for Gamma Hanto.
 * @author aissayev
 *
 */
public class DeltaHantoMasterTest {
	
	/**
	 * Move Data for testing from professors tests
	 * @author aissayev
	 *
	 */
	class MoveData {
		final HantoPieceType type;
		final HantoCoordinate from, to;
		
		private MoveData(HantoPieceType type, HantoCoordinate from, HantoCoordinate to) 
		{
			this.type = type;
			this.from = from;
			this.to = to;
		}
	}
	
	/**
	 * Internal class for these test cases
	 */
	public class TestHantoCoordinate implements HantoCoordinate
	{
		private final int x,y;
		
		/**
		 * Constructor for test Hanto Coordinate
		 * @param x
		 * @param y
		 */
		public TestHantoCoordinate(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
		/*
		 * @see hanto.common.HantoCoordinate#getX()
		 */
		@Override
		public int getX()
		{
			return x;
		}

		/*
		 * @see hanto.common.HantoCoordinate#getY()
		 */
		@Override
		public int getY()
		{
			return y;
		}		
	}

	private static HantoGameFactory factory = HantoGameFactory.getInstance();
	private HantoGame game;
	
	/**
	 * Initialize the game before testing
	 */
	@BeforeClass
	public static void initializeClass()
	{
		factory = HantoGameFactory.getInstance();
	}
	
	/**
	 * game setup
	 */
	@Before
	public void setup()
	{
		game = factory.makeHantoGame(HantoGameID.DELTA_HANTO, BLUE);
	}
	
	/**
	 * red place butterfly
	 * @throws HantoException
	 */
	@Test	// 1
	public void redPlacesButterflyFirst() throws HantoException
	{
		game = factory.makeHantoGame(HantoGameID.DELTA_HANTO, RED);
		final MoveResult mr = game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		assertEquals(OK, mr);
		final HantoPiece p = game.getPieceAt(makeCoordinate(0,0));
		assertEquals(RED, p.getColor());
		assertEquals(BUTTERFLY, p.getType());
	}
	
	/**
	 * Master Test from grading
	 * @throws HantoException
	 */
	@Test	// 2
	public void canPlaceCrab() throws HantoException
	{
		// turn 1
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		// turn 2 blue resigns
		game.makeMove(CRAB, null, makeCoordinate(0, -1));
		final HantoPiece p = game.getPieceAt(makeCoordinate(0, -1));
		assertEquals(BLUE, p.getColor());
		assertEquals(CRAB, p.getType());
		
	}
	
	/**
	 * blue place crab first at origin
	 * @throws HantoException
	 */
	@Test	// 3
	public void bluePlacesCrabFirst() throws HantoException
	{
		final MoveResult mr = game.makeMove(CRAB, null, makeCoordinate(0,0));
		assertEquals(OK, mr);
		final HantoPiece p = game.getPieceAt(makeCoordinate(0,0));
		assertEquals(BLUE, p.getColor());
		assertEquals(CRAB, p.getType());
	}
	
	/**
	 * blue resigns from the game
	 * @throws HantoException
	 */
	@Test	// 4
	public void blueResigns() throws HantoException
	{
		// turn 1
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		// turn 2 blue resigns
		assertEquals(RED_WINS, game.makeMove(null, null, null));
	}
	
	/**
	 * Make red sparrow fly
	 * @throws HantoException
	 */
	@Test	// 5
	public void blueFlySparrow() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(SPARROW, null, makeCoordinate(1,1));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,2));
		assertEquals(OK, game.makeMove(SPARROW, makeCoordinate(1,-1), makeCoordinate(-1,1)));
		
	}
	
	/**
	 * Fly piece from empty hex
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 6
	public void movePieceFormEmptyHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(SPARROW, makeCoordinate(0,2), makeCoordinate(1,0));
		assertTrue(true);	// code audit
	}
	
	/**
	 * Fly that disconnects the group
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 7
	public void flyDisconnects() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(SPARROW, null, makeCoordinate(0,2));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		assertEquals(null, game.makeMove(SPARROW, makeCoordinate(0,1), makeCoordinate(1,-2)));

	}
	
	/**
	 * red resigns from the game, blue makes move
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 8
	public void redResignsBlueMakesMoves() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		assertEquals(BLUE_WINS, game.makeMove(null, null, null));
		assertNull(game.makeMove(SPARROW, makeCoordinate(0,1), makeCoordinate(1,0)));
		
	}
	
	/**
	 * Crab Walks
	 * @throws HantoException
	 */
	@Test	// 9
	public void crabWalksOneHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(-1,2)));	
	}
	
	/**
	 * Crab Walks
	 * @throws HantoException
	 */
	@Test	// 10
	public void crabWalksTwoHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(-1,1)));	
	}
	
	/**
	 * Crab Walks
	 * @throws HantoException
	 */
	@Test	// 11
	public void crabWalksThreeHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(OK, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(-2,1)));	
	}
	
	/**
	 * Crab Walks more than it can
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 12
	public void crabWalksFourHex() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(null, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(-2,0)));	
	}
	
	/**
	 * Crab Walks and disconnects
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 13
	public void crabWalksDisconnects() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(null, game.makeMove(CRAB, makeCoordinate(0,2), makeCoordinate(2,1)));	
	}
	
	/**
	 * Butterfly walks more than it can
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 14
	public void butterflyWalksMoreThanOne() throws HantoException
	{
		game.makeMove(CRAB, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(null, game.makeMove(BUTTERFLY, makeCoordinate(0,2), makeCoordinate(1,0)));	
	}
	
	/**
	 * Butterfly walks more than it can
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 15
	public void butterflySamePlaceMove() throws HantoException
	{
		game.makeMove(CRAB, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(0,1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,2));
		game.makeMove(CRAB, null, makeCoordinate(1,-1));
		assertEquals(null, game.makeMove(BUTTERFLY, makeCoordinate(0,2), makeCoordinate(0,2)));	
	}
	
	/**
	 * Crab Walks and blue wins
	 * @throws HantoException
	 */
	@Test	// 16
	public void crabWalksBlueWins() throws HantoException
	{
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(SPARROW, null, makeCoordinate(1,-1));
		game.makeMove(SPARROW, null, makeCoordinate(-1,0));
		game.makeMove(CRAB, null, makeCoordinate(2,-1));
		game.makeMove(CRAB, null, makeCoordinate(-1,-1));
		game.makeMove(CRAB, null, makeCoordinate(1,0));
		game.makeMove(CRAB, null, makeCoordinate(0,-2));
		assertEquals(BLUE_WINS, game.makeMove(CRAB, makeCoordinate(2,-1), makeCoordinate(1,-2)));	
	}
	
	/**
	 * Try move trapped piece
	 * @throws HantoException
	 */
	@Test(expected = HantoException.class)	// 17
	public void blueTrappedMove () throws HantoException 
	{
		game.makeMove(SPARROW, null, makeCoordinate(0,0));
		game.makeMove(BUTTERFLY, null, makeCoordinate(0,-1));
		game.makeMove(BUTTERFLY, null, makeCoordinate(1,0));
		game.makeMove(SPARROW, null, makeCoordinate(-1,-1));
		game.makeMove(CRAB, null, makeCoordinate(-1,1));
		game.makeMove(CRAB, null, makeCoordinate(0,-2));
		assertEquals(null, game.makeMove(SPARROW, makeCoordinate(0,0), makeCoordinate(-1,0)));
	}
	
	/**
	 * Game no turn limit, game exceeds 20 turns, and still playable
	 * @throws HantoException
	 */
	@Test	// 18
	public void notDrawAfterTwentyTurns() throws HantoException
	{
		MoveResult mr = makeMoves(
				md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(SPARROW, 1, -1), md(SPARROW, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2),
				md(SPARROW, 0, -1, 1, -1), md(SPARROW, 0, 2, -1, 2),
				md(SPARROW, 1, -1, 0, -1), md(SPARROW, -1, 2, 0, 2));
		assertEquals(OK, mr);
	}
	
	//#################### Master Tests #########################
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void bluePlacesButterflyFirst() throws HantoException
	{
		final MoveResult mr = game.makeMove(BUTTERFLY, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
		checkPieceAt(0, 0, BLUE, BUTTERFLY);
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void redPlacesSparrowFirst() throws HantoException
	{
		game = factory.makeHantoGame(HantoGameID.GAMMA_HANTO, RED);
		final MoveResult mr = game.makeMove(SPARROW, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void bluePlacesCrabFirst2() throws HantoException
	{
		final MoveResult mr = game.makeMove(CRAB, null, makeCoordinate(0, 0));
		assertEquals(OK, mr);
		checkPieceAt(0, 0, BLUE, CRAB);
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void blueMovesCrab1() throws HantoException
	{
		final MoveResult mr = makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(CRAB, 0, -1),
				md(CRAB, 0, 2), md(CRAB, 0, -1, -1, 0));
		assertEquals(OK, mr);
		checkPieceAt(-1, 0, BLUE, CRAB);
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void moveToDisconnectConfiguration() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(BUTTERFLY, 0, 0, 0, -1));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void moveButterflyToSameHex() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(BUTTERFLY, 0, 0, 0, 0));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void moveCrabToOccupiedHex() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(CRAB, 0, -1),
				md(CRAB, 0, 2), md(CRAB, 0, -1, 0, 0));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void moveFromEmptyHex() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(BUTTERFLY, 1, 0, 1, -1));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToMoveTooFar() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(BUTTERFLY, 0, 0, -1, 2));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToMoveWrongPieceType() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(CRAB, 0, -1),
				md(CRAB, 0, 2), md(SPARROW, 0, -1, -1, 0));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToMoveWrongColorPiece() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(CRAB, 0, -1),
				md(CRAB, 0, 2), md(CRAB, 0, 2, 1, 1));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToMoveWhenNotEnoughSpace() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), 
				md(CRAB, -1, 0), md(CRAB, 0, 2),
				md(CRAB, 1, -1), md(CRAB, 0, 3),
				md(BUTTERFLY, 0, 0, 0, -1));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToUseTooManyButterflies() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), md(BUTTERFLY, 0, -1));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToUseTooManySparrows() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), 
				md(SPARROW, 0, -1), md(SPARROW, 0, 2),
				md(SPARROW, 0, -2), md(SPARROW, 0, 3),
				md(SPARROW, 0, -3), md(SPARROW, 0, 4),
				md(SPARROW, 0, -4), md(SPARROW, 0, 5),
				md(SPARROW, 0, -5));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToUseTooManyCrabs() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), 
				md(CRAB, 0, -1), md(CRAB, 0, 2),
				md(CRAB, 0, -2), md(CRAB, 0, 3),
				md(CRAB, 0, -3), md(CRAB, 0, 4),
				md(CRAB, 0, -4), md(CRAB, 0, 5),
				md(CRAB, 0, -5));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToUsePieceNotInGame() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1), 
				md(CRANE, 0, -1));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void blueWins() throws HantoException
	{
		MoveResult mr = makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(CRAB, -1, 0), md(SPARROW, 1, 1),
				md(CRAB, 1, -1), md(SPARROW, 0, 2),
				md(CRAB, 1, -1, 1, 0), md(SPARROW, -1, 2),
				md(CRAB, -1, 0, -1, 1));
		assertEquals(BLUE_WINS, mr);
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void redSelfLoses() throws HantoException
	{
		MoveResult mr = makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(CRAB, -1, 0), md(SPARROW, 0, 2),
				md(CRAB, 1, -1), md(CRAB, 1, 2),
				md(CRAB, 1, -1, 1, 0), md(CRAB, -1, 2),
				md(CRAB, -1, 0, -1, 1), md(CRAB, 1, 2, 1, 1));
		assertEquals(BLUE_WINS, mr);
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToPlacePieceNextToOpponent() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(SPARROW, -1, 0), md(SPARROW, -2, 0));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void walkTwoHexes() throws HantoException
	{
		MoveResult mr = makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(CRAB, 0, -1), md(CRAB, 0, 2),
				md(CRAB, 0, -1, 1, 0));
		checkPieceAt(1, 0, BLUE, CRAB);
		assertNull(game.getPieceAt(makeCoordinate(0, -1)));
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void walkThreeHexes() throws HantoException
	{
		MoveResult mr = makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(CRAB, 0, -1), md(CRAB, 0, 2),
				md(CRAB, 0, -1, -1, 2));
		checkPieceAt(-1, 2, BLUE, CRAB);
		assertNull(game.getPieceAt(makeCoordinate(0, -1)));
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void attemptToWalkFourHexes() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(CRAB, 0, -1), md(CRAB, 0, 2),
				md(CRAB, 0, -1, -1, 3));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void walkThreeHexesAndDisconnectConfiguration() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(CRAB, 0, -1), md(CRAB, 0, 2),
				md(CRAB, 0, -1, 3, -2));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void butterflyNotPlacedByFourthMoveByFirstPlayer() throws HantoException
	{
		makeMoves(md(SPARROW, 0, 0), md(SPARROW, 0, 1),
				md(SPARROW, 0, -1), md(SPARROW, 0, 2),
				md(SPARROW, 0, -2), md(SPARROW, 0, 3),
				md(SPARROW, 0, -3));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void butterflyNotPlacedByFourthMoveBySecondPlayer() throws HantoException
	{
		makeMoves(md(SPARROW, 0, 0), md(SPARROW, 0, 1),
				md(BUTTERFLY, 0, -1), md(SPARROW, 0, 2),
				md(SPARROW, 0, -2), md(SPARROW, 0, 3),
				md(SPARROW, 0, -3), md(SPARROW, 0, 4));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void tryToMoveAfterGameIsOver() throws HantoException
	{
		makeMoves(md(BUTTERFLY, 0, 0), md(BUTTERFLY, 0, 1),
				md(SPARROW, -1, 0), md(SPARROW, 1, 1),
				md(SPARROW, 1, -1), md(SPARROW, 0, 2),
				md(SPARROW, 1, -1, 1, 0), md(SPARROW, -1, 2),
				md(SPARROW, -1, 0, -1, 1), md(SPARROW, 0, 3));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test(expected=HantoException.class)
	public void attemptToMoveBeforeButterflyIsOnBoard() throws HantoException
	{
		makeMoves(md(CRAB, 0, 0), md (BUTTERFLY, 0, 1),
				md(CRAB, 0, 0, 1, 0));
		assertTrue(true); // code audit
	}
	
	/**
	 * professor's test
	 * @throws HantoException
	 */
	@Test
	public void blueResignsOnFirstMove() throws HantoException
	{
		assertEquals(RED_WINS, game.makeMove(null, null, null));
	}
	
	/**
	 * Make sure that the piece at the location is what you expect
	 * @param x x-coordinate
	 * @param y y-coordinate
	 * @param color piece color expected
	 * @param type piece type expected
	 */
	private void checkPieceAt(int x, int y, HantoPlayerColor color, HantoPieceType type)
	{
		final HantoPiece piece = game.getPieceAt(makeCoordinate(x, y));
		assertEquals(color, piece.getColor());
		assertEquals(type, piece.getType());
	}
	
	/**
	 * Make a MoveData object given the piece type and the x and y coordinates of the
	 * desstination. This creates a move data that will place a piece (source == null)
	 * @param type piece type
	 * @param toX destination x-coordinate
	 * @param toY destination y-coordinate
	 * @return the desitred MoveData object
	 */
	private MoveData md(HantoPieceType type, int toX, int toY) 
	{
		return new MoveData(type, null, makeCoordinate(toX, toY));
	}
	
	private MoveData md(HantoPieceType type, int fromX, int fromY, int toX, int toY)
	{
		return new MoveData(type, makeCoordinate(fromX, fromY), makeCoordinate(toX, toY));
	}
	
	/**
	 * Make the moves specified. If there is no exception, return the move result of
	 * the last move.
	 * @param moves
	 * @return the last move result
	 * @throws HantoException
	 */
	private MoveResult makeMoves(MoveData... moves) throws HantoException
	{
		MoveResult mr = null;
		for (MoveData md : moves) {
			mr = game.makeMove(md.type, md.from, md.to);
		}
		return mr;
	}

	
	
	// Helper methods
	private HantoCoordinate makeCoordinate(int x, int y)
	{
		return new TestHantoCoordinate(x, y);
	}

}
