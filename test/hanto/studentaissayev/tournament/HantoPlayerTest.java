/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.tournament;

import static org.junit.Assert.*;
import static hanto.common.HantoPieceType.*;

import java.lang.reflect.Field;

import org.junit.Test;

import hanto.common.HantoCoordinate;
import hanto.common.HantoGameID;
import hanto.common.HantoPlayerColor;
import hanto.studentaissayev.epsilon.EpsilonHantoGame;
import hanto.tournament.HantoMoveRecord;

/**
 * Test for HantoPlayer
 * @author aissayev
 *
 */
public class HantoPlayerTest {

	/**
	 * Internal class for these test cases
	 */
	public class TestHantoCoordinate implements HantoCoordinate
	{
		private final int x,y;
		
		/**
		 * Constructor for test Hanto Coordinate
		 * @param x
		 * @param y
		 */
		public TestHantoCoordinate(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
		/*
		 * @see hanto.common.HantoCoordinate#getX()
		 */
		@Override
		public int getX()
		{
			return x;
		}

		/*
		 * @see hanto.common.HantoCoordinate#getY()
		 */
		@Override
		public int getY()
		{
			return y;
		}		
	}
	
	/**
	 * Can start the game
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	@Test	// 1
	public void startTheGame() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
	{
		Field f = HantoPlayer.class.getDeclaredField("game");
		f.setAccessible(true);
		EpsilonHantoGame game;
		
		HantoPlayer hp1 = new HantoPlayer();
		hp1.startGame(HantoGameID.EPSILON_HANTO, HantoPlayerColor.RED, false);
		game = (EpsilonHantoGame) f.get(hp1);
		assertTrue(game.getPlayer() == HantoPlayerColor.BLUE);
	}
	
	/**
	 * Can start the game going first
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 * @throws NoSuchFieldException
	 * @throws SecurityException
	 */
	@Test	// 2
	public void startTheGameGoingFirst() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
	{
		Field f = HantoPlayer.class.getDeclaredField("game");
		f.setAccessible(true);
		EpsilonHantoGame game;
		
		HantoPlayer hp1 = new HantoPlayer();
		hp1.startGame(HantoGameID.EPSILON_HANTO, HantoPlayerColor.BLUE, true);
		game = (EpsilonHantoGame) f.get(hp1);
		assertTrue(game.getPlayer() == HantoPlayerColor.BLUE);
	}
	
	/**
	 * Can switch players
	 */
	@Test
	public void switchColorTest()
	{
		HantoPlayer hp = new HantoPlayer();
		assertEquals(hp.switchColor(HantoPlayerColor.RED), HantoPlayerColor.BLUE);
		assertEquals(hp.switchColor(HantoPlayerColor.BLUE), HantoPlayerColor.RED);
	}
	
	/**
	 * record opponents move
	 */
	@Test
	public void opponentMove()
	{
		HantoPlayer hp1 = new HantoPlayer();
		hp1.startGame(HantoGameID.EPSILON_HANTO, HantoPlayerColor.RED, false);
		HantoMoveRecord mr = new HantoMoveRecord(BUTTERFLY, null, makeCoordinate(0,0));
		assertNotNull(hp1.makeMove(mr));
		
	}
	
	// Helper methods
	private HantoCoordinate makeCoordinate(int x, int y)
	{
		return new TestHantoCoordinate(x, y);
	}
}
