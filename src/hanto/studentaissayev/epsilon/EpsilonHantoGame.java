/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.epsilon;

import static hanto.common.HantoPlayerColor.BLUE;
import static hanto.common.MoveResult.BLUE_WINS;
import static hanto.common.MoveResult.RED_WINS;

import java.util.Collection;
import hanto.common.HantoCoordinate;
import hanto.common.HantoGameID;
import hanto.common.HantoPlayerColor;
import hanto.common.HantoPrematureResignationException;
import hanto.common.MoveResult;
import hanto.studentaissayev.common.HantoGameBase;
import hanto.studentaissayev.common.HantoPlayerFactory;

/**
 * Epsilon variation of Hanto Game. With more pieces
 * and different strategies of piece moves 
 * 
 * @author aissayev
 *
 */
public class EpsilonHantoGame extends HantoGameBase{

	/**
	 * Constructor for Epsilon game
	 * @param movesFirst hanto player who makes first move
	 */
	public EpsilonHantoGame(HantoPlayerColor movesFirst) {
		super(movesFirst);
		bluePlayer = HantoPlayerFactory.makePlayer(HantoGameID.EPSILON_HANTO, HantoPlayerColor.BLUE);
		redPlayer = HantoPlayerFactory.makePlayer(HantoGameID.EPSILON_HANTO, HantoPlayerColor.RED);
		currentPlayer = movesFirst == HantoPlayerColor.BLUE ? bluePlayer : redPlayer;
		canResign = true;
	}

	@Override
	public HantoGameID getHantoGameID() {
		return HantoGameID.EPSILON_HANTO;
	}

	/**
	 * if resigns is requested opponent player wins
	 * @return MoveResult for resignation
	 * @throws HantoPrematureResignationException 
	 */
	@Override
	protected MoveResult handleResignation() throws HantoPrematureResignationException 
	{
		if(movesAvailable())
		{
			throw new HantoPrematureResignationException();
		}
		MoveResult result;
		if(onMove == BLUE) {
			result = RED_WINS;
		} else {
			result = BLUE_WINS;
		}
		gameOver = true;
		return result;
	}
	
	/**
	 * if player has moves available
	 * @return true if moves are available
	 */
	public boolean movesAvailable()
	{
		boolean result = true;
		Collection<HantoCoordinate> piecesOnCoordinates = currentPlayer.getPositions();
		if(!currentPlayer.getInventorySize())
		{
			result = true;
		} else {
			result = false;
		}
		return result;
	}
	

}
