/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/

package hanto.studentaissayev.beta;

import hanto.common.*;
import hanto.studentaissayev.common.HantoGameBase;
import hanto.studentaissayev.common.HantoPlayerFactory;

/**
 * Beta variation of Hanto Game. Game last for 12 turns. Each player
 * can have two game pieces, butterfly and sparrow.
 * @version Mar 16, 2016
 */
public class BetaHantoGame extends HantoGameBase
{
	
	/**
	 * Constructor for Beta Hanto Game. Set max turns to twelve
	 * and creates two players
	 * @param movesFirst
	 */
	
	public BetaHantoGame (HantoPlayerColor movesFirst) {
		
		super(movesFirst);
		maxTurns = 12; // 6 full turns
		bluePlayer = HantoPlayerFactory.makePlayer(HantoGameID.BETA_HANTO, HantoPlayerColor.BLUE);
		redPlayer = HantoPlayerFactory.makePlayer(HantoGameID.BETA_HANTO, HantoPlayerColor.RED);
		currentPlayer = movesFirst == HantoPlayerColor.BLUE ? bluePlayer : redPlayer;
		canResign = false;
	}
	
	@Override 
	public MoveResult  makeMove (HantoPieceType pieceType, HantoCoordinate from, HantoCoordinate to) throws HantoException
	{
		if(from != null) {
			throw new HantoException ("Can not move pieces in Beta");
		}
		return super.makeMove(pieceType, from, to);
	}
	
	/**
	 * Validation method to check validity of move destination
	 * 
	 * 
	 * @param from	hanto coordination from where piece is moving from
	 * @throws HantoException
	 */
	@Override
	protected void checkDestination(HantoCoordinate from) throws HantoException 
	{
	
		if(turn == 1 && onMove == movesFirst) {
			if(locationTo.getX() != 0 || locationTo.getY() != 0) {
				throw new HantoException("You must make move to origin");
			}
		} else {
			if(board.containsKey(locationTo)) {
				throw new HantoException("Location is already in use by other piece");
			}
			
			if(!checkAdjacency()) {
				throw new HantoException("Destination is not adjacent to anything");
			}
			
		}
	}
	


	



}
