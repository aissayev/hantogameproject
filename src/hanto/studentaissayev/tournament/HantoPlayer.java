/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package hanto.studentaissayev.tournament;
import hanto.common.*;
import hanto.studentaissayev.epsilon.EpsilonHantoGame;
import hanto.tournament.*;

/**
 * Hanto player that playes the game
 * @version Oct 13, 2014
 */
public class HantoPlayer implements HantoGamePlayer
{
	private EpsilonHantoGame game;
	private int turnCount = 0;
	

	@Override
	public void startGame(HantoGameID version, HantoPlayerColor myColor,
			boolean doIMoveFirst)
	{
		if (doIMoveFirst) {
			game = new EpsilonHantoGame(myColor);
		} else {
			game = new EpsilonHantoGame(switchColor(myColor));
		}
	}

	/**
	 * Switch hanto player  
	 * @param color hanto player color
	 * @return Hanto Player Color
	 */
	public HantoPlayerColor switchColor(HantoPlayerColor color)
	{
		HantoPlayerColor pc = null;
		switch(color)
		{
			case BLUE:
				pc = HantoPlayerColor.RED;
				break;
			case RED:
				pc = HantoPlayerColor.BLUE;
				break;
		}
		return pc;
	}
	
	/*
	 * @see hanto.tournament.HantoGamePlayer#makeMove(hanto.tournament.HantoMoveRecord)
	 */
	@Override
	public HantoMoveRecord makeMove(HantoMoveRecord opponentsMove)
	{
		try
		{
			if(opponentsMove != null)
			{
				game.makeMove(opponentsMove.getPiece(), opponentsMove.getFrom(), opponentsMove.getTo());
			}
		} catch (HantoException e) {
			return new HantoMoveRecord(null, null, null);
		}
		
		return new HantoMoveRecord(null, null, null);
	}
	
	

}
