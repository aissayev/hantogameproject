/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common.movement;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import hanto.common.HantoCoordinate;
import hanto.common.HantoPiece;
import hanto.studentaissayev.common.HantoCoordinateImpl;
import hanto.studentaissayev.common.HantoGameBase;

/**
 * Fly Validotor move strategy. Set of rules for jump move
 * @author aissayev
 *
 */
public class JumpValidator extends MoveStrategy {
	

	@Override
	protected boolean validateMove(HantoPiece piece, HantoCoordinate from, HantoCoordinate to, HantoGameBase game) {
		
		boolean result = false;
		if (checkLinearMove(from, to)) {
			result = checkPiecesInBetween(from, to, game);
		}
		
		return result;

	}
	
	/**
	 * Validation method to check if the move from one HantoCoordinate to
	 * another is valid
	 * 
	 * @param from position from 
	 * @param to positions to
	 * @return true if valid
	 */
	private boolean checkLinearMove(HantoCoordinate from, HantoCoordinate to)
	{
		int deltaX = to.getX() - from.getX();
		int deltaY = to.getY() - from.getY();
		if(deltaX == 0 || deltaY == 0) {
			return true;
		}
		
		return false;	
	}
	
	/**
	 * Validation for exitance of pieces in between the move path
	 * 
	 * @param from	position from
	 * @param to	position to
	 * @param game	basic Hanto rule set
	 * @return true if valid
	 */
	private boolean checkPiecesInBetween(HantoCoordinate from, HantoCoordinate to, HantoGameBase game) 
	{
		Collection<HantoCoordinate> hcInBetween = getCoordinatesInBetween(from, to);
		
		return hcInBetween.size() != 0 && checkNotEmpty(hcInBetween, game);
	}
	
	/**
	 * Get collections of the Hanto Coordinates during the move path
	 * 
	 * @param from position from
	 * @param to position to
	 * @return collection of hanto coordinate in between from and to
	 */
	private Collection<HantoCoordinate> getCoordinatesInBetween(HantoCoordinate from, HantoCoordinate to)
	{
		Set<HantoCoordinate> pieces = new HashSet<HantoCoordinate>();
		int deltaX = to.getX() - from.getX();
		int deltaY = to.getY() - from.getY();
		int distance = Math.max(Math.abs(deltaX), Math.abs(deltaY));
		if(distance > 1) 
		{
			int x = deltaX /distance;
			int y = deltaY/distance;
			
			for(int i=1; i < distance; i++)
			{
				pieces.add(new HantoCoordinateImpl(to.getX() - (i*x), to.getY() - (i*y)));
			}
		}
		return pieces;
	}
	
	/**
	 * Validation for collection of Hanto Coordinates for null
	 * 
	 * @param set	collection of Hanto Coordinates
	 * @param game	basuc Hanto game set of rules
	 * @return
	 */
	private boolean checkNotEmpty(Collection<HantoCoordinate> set, HantoGameBase game)
	{
		
		for(HantoCoordinate hc: set)
		{
			if(game.getPieceAt(hc) == null)
			{
				return false;
			}
		}
		
		return true;
	}

}