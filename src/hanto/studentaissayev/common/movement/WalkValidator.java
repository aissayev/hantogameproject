/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common.movement;

import hanto.common.HantoCoordinate;
import hanto.common.HantoGameID;
import hanto.common.HantoPiece;
import hanto.common.HantoPieceType;
import hanto.studentaissayev.common.HantoGameBase;

/**
 * Walkt Validotor move strategy. Set of rules for walk move
 * @author aissayev
 *
 */
public class WalkValidator extends MoveStrategy {



	@Override
	public boolean validateMove(HantoPiece piece, HantoCoordinate from, HantoCoordinate to, HantoGameBase game) {
		
		
		int distance = getDistance(from, to);
		HantoPieceType type = piece.getType();
		int maxDistance;
		boolean result = false;
		switch(type)
		{
			case BUTTERFLY:
				maxDistance = 1;
				break;
			case CRAB:
				if(game.getHantoGameID() == HantoGameID.DELTA_HANTO) {
					maxDistance = 3;
				} else {
					maxDistance = 1;
				}
				break;
			default:
				maxDistance = 0;
		}
		if(distance == 0) {
			return false;
			
		} else if (distance <= maxDistance ) {
			result = !isHexTrapped(from);
		}
		return result;
	}


	
}
