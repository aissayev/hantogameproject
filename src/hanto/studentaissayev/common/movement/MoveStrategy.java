/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common.movement;

import java.util.Map;
import hanto.common.HantoCoordinate;
import hanto.common.HantoPiece;

import hanto.studentaissayev.common.HantoCoordinateImpl;
import hanto.studentaissayev.common.HantoGameBase;

/**
 * Move Strategy validator template class for different move strategies
 * @author aissayev
 *
 */
public abstract class MoveStrategy {
	
	private HantoGameBase game;
	
	/**
	 * checks if given move is possible
	 * @param game	basic Hanto game set of rules
	 * @param from	position from
	 * @param to	position to
	 * @return boolean	true if piece can make a move
	 */
	public boolean canMove(HantoGameBase game,
			HantoCoordinate from, HantoCoordinate to) {
		this.game = game;
		HantoPiece piece = game.getPieceAt(from);
		return validateMove(piece, from, to, game);
	}
	
	/**
	 * Validation of the current move
	 * 
	 * @param piece	piece that is being moved
	 * @param from	position from
	 * @param to	position to	
	 * @param game	basic Hato game set of rules
	 * @return boolean	true if given move is valid
	 */
	protected abstract boolean validateMove(HantoPiece piece, HantoCoordinate from, HantoCoordinate to, HantoGameBase game);
	
	/**
	 * get distance between hexes
	 *  
	 * @param from position from
	 * @param to   position to
	 * @return Integer distance value
	 */
	protected int getDistance(HantoCoordinate from, HantoCoordinate to) 
	{
		
		int distance;
		int deltaX = from.getX() - to.getX();
		int deltaY = from.getY() - to.getY();
		if(deltaX * deltaY > 0) {
			distance = Math.abs(deltaX + deltaY);
		} else {
			distance = Math.max(Math.abs(deltaX), Math.abs(deltaY));
		}
		return distance;
	}


	/**
	 * Validation if peace is trapped by the move
	 * 
	 * @param hc  Hanto Coordinate location of the piece
	 * @return boolean	true if piece is trapped at the location
	 */
	protected boolean isHexTrapped (HantoCoordinate hc)
	{
		boolean result = false;
		Map<HantoCoordinate, HantoPiece> board = game.getBoard();
		int x = hc.getX();
		int y = hc.getY();
		
		if(board.containsKey(new HantoCoordinateImpl(x, y+1)) 
			&&!board.containsKey(new HantoCoordinateImpl(x+1, y))
			&& board.containsKey(new HantoCoordinateImpl(x+1, y-1))
			&& !board.containsKey(new HantoCoordinateImpl(x, y-1))
			&& board.containsKey(new HantoCoordinateImpl(x-1, y))
			&& !board.containsKey(new HantoCoordinateImpl(x-1, y+1))) 
		{
			result = true;
			
		} else if (!board.containsKey(new HantoCoordinateImpl(x, y+1)) 
				&& board.containsKey(new HantoCoordinateImpl(x+1, y))
				&& !board.containsKey(new HantoCoordinateImpl(x+1, y-1))
				&& board.containsKey(new HantoCoordinateImpl(x, y-1)))
				//&& !board.containsKey(new HantoCoordinateImpl(x-1, y))
				//&& board.containsKey(new HantoCoordinateImpl(x-1, y+1)))
		{
			result = true;
		}
		
		return result;
		
	}
	
	 
	
	
}
