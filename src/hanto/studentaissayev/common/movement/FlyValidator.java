/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common.movement;

import hanto.common.HantoCoordinate;
import hanto.common.HantoGameID;
import hanto.common.HantoPiece;
import hanto.studentaissayev.common.HantoGameBase;

/**
 * Fly Validotor move strategy. Set of rules fro fly move
 * @author aissayev
 *
 */
public class FlyValidator extends MoveStrategy {

	@Override
	protected boolean validateMove(HantoPiece piece, HantoCoordinate from, HantoCoordinate to, HantoGameBase game) {
		
		int distance = getDistance(from, to);
		int maxDist;
		boolean result = true;
		if(game.getHantoGameID() == HantoGameID.EPSILON_HANTO) {
			if(distance > 5) {
				result = false;
			}
		}
		return result;

	}

}
