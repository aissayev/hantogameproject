/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common.movement;

import hanto.common.HantoPieceType;


/**
 * Move Strategy generates move strategy for the piece
 * @author aissayev
 *
 */
public class MoveStrategyFactory {
	
	/**
	 * creates move strategy
	 * @param type	Hanto Piece type
	 * @return MoveStrategy	move strategy for the given hanto piece
	 */
	public static MoveStrategy getMoveValidator(HantoPieceType type)
	{
		switch(type)
		{
			case BUTTERFLY:
				return new WalkValidator();
			case SPARROW:
				return new FlyValidator();
			case CRAB:
				return new WalkValidator();
			case HORSE:
				return new JumpValidator();
			default:
				return null;
		
		}
	}

}
