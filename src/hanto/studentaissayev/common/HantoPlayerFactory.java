/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common;

import hanto.common.*;

import static hanto.common.HantoPieceType.*;
import static hanto.common.HantoGameID.*;

/**
 * Hanto Player Factory generates player for the game
 * @author aissayev
 *
 */
public class HantoPlayerFactory {

	/**
	 * Make new player
	 * @param version hanto game variation
	 * @param color color for hanto player
	 * @return HantoPlayer
	 */
	public static HantoPlayer makePlayer(HantoGameID version, HantoPlayerColor color)
	{
		HantoPlayer player = new HantoPlayer(color);
		try {
			switch(version) {
				case BETA_HANTO:
					player.setPlayerPieces(SPARROW, 5);
					break;
				case GAMMA_HANTO:
					player.setPlayerPieces(SPARROW, 5);
					break;
				case DELTA_HANTO:
					player.setPlayerPieces(SPARROW, 4);
					player.setPlayerPieces(CRAB, 4);
					break;
				case EPSILON_HANTO:
					player.setPlayerPieces(SPARROW, 2);
					player.setPlayerPieces(CRAB, 6);
					player.setPlayerPieces(HORSE, 4);
					break;
				default:
					player = null;
			}
		} catch (HantoException he) {
			player = null;
		}
		return player;
	}
}
