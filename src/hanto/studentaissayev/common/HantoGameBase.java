/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common;

import hanto.common.*;
import hanto.studentaissayev.common.movement.MoveStrategyFactory;

import static hanto.common.MoveResult.*;
import static hanto.common.HantoPlayerColor.*;
import static hanto.common.HantoPieceType.*;
import static hanto.studentaissayev.common.HantoPieceFactory.makePiece;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import java.util.Set;

/**
 * Abstract class for all version of Hanto game
 * @author aissayev
 *
 */
public abstract class HantoGameBase implements HantoGame {

	protected final HantoPlayerColor movesFirst;
	protected final Map<HantoCoordinate, HantoPiece> board;
	protected final Map<HantoCoordinate, ArrayList<HantoCoordinate>> hexGrid;
	protected final Set<HantoCoordinate> playableHexes = new HashSet<HantoCoordinate>();
	protected HantoPlayerColor onMove;
	protected HantoPieceType pieceOnMove;
	protected int maxTurns;
	protected int turn;
	protected boolean canResign;
	protected HantoCoordinateImpl blueButterflyLocation, redButterflyLocation;
	protected HantoCoordinateImpl locationFrom, locationTo;
	protected HantoPlayer bluePlayer, redPlayer, currentPlayer;
	
	
	
	protected boolean gameOver;
	
	
	/**
	 * Hanto Base Game constructor
	 * @param movesFirst player color who makes first move
	 */
	protected HantoGameBase (HantoPlayerColor movesFirst)
	{
		this.movesFirst = movesFirst;
		onMove = movesFirst;
		board = new HashMap<HantoCoordinate, HantoPiece>();
		hexGrid = new HashMap<HantoCoordinate, ArrayList<HantoCoordinate>>(); 
		blueButterflyLocation = null;
		redButterflyLocation = null;
		locationFrom = null;
		locationTo = null;
		pieceOnMove = null;
		turn = 1;
		maxTurns = 2147483647;
		gameOver = false;
		
	}
	
	/**
	 * make move for the given Hanto piece type in locaiton from and
	 * location to
	 * 
	 * @param pieceType	Hanto piece type that is being moved
	 * @param from	location from
	 * @param to	location to
	 * @return MoveResult	game result from the move
	 */
	public MoveResult  makeMove (HantoPieceType pieceType, HantoCoordinate from, HantoCoordinate to) throws HantoException
	{
		
		if (gameOver) {
			throw new HantoException("You cannot move after the game is finished");
		}
		
		if(canResign && checkResignnation(pieceType, from, to)) {
			return handleResignation();
		}
		pieceOnMove = pieceType;
		

		
		setToFrom(to, from);
		checkEmptyHex();
		checkCorrectPiece();
		checkDestination(from);
		checkButterflyByFourthTurn();
		moveValidate(from);
		recordMove();


		
		MoveResult result = gameState();
		turn++;
		onMove = onMove == BLUE ? RED : BLUE;
		currentPlayer = currentPlayer == bluePlayer ? redPlayer : bluePlayer;
		// reset location markers
		locationFrom = null;
		locationTo = null;
		return result;
	}
	
	/**
	 * Checks if actual move is valid and does not brake game rules 
	 * 
	 * @param from location form
	 * @throws HantoException
	 */
	protected void moveValidate(HantoCoordinate from) throws HantoException 
	{
		if(from != null) {
			if(!MoveStrategyFactory.getMoveValidator(pieceOnMove).canMove(this, locationFrom, locationTo))
			{
				throw new HantoException("Move is not valid");
			}
			if(!isConnected()) {
				throw new HantoException("Graph is disconnected");
			}
		}
	}
	
	/**
	 * Validation method for destination move check
	 * 
	 * @param from location from
	 * @throws HantoException
	 */
	protected void checkDestination(HantoCoordinate from) throws HantoException 
	{
	
		if(turn == 1 && onMove == movesFirst) {
			if(locationTo.getX() != 0 || locationTo.getY() != 0) {
				throw new HantoException("You must make move to origin");
			}
		} else {
			if(board.containsKey(locationTo)) {
				throw new HantoException("Location is already in use by other piece");
			}
			
			if(!checkAdjacency()) {
				throw new HantoException("Destination is not adjacent to anything");
			}
			
			if(from == null && turn > 2 && !checkPlacementFriendly()) {
				throw new HantoException("Destination is not ajacent to friendly pieces");
			}
			
			if(from != null && pieceOnMove != BUTTERFLY) {
				checkSparrowMoveBeforeButterfly();
			}
			
		}
	}
	
	/**
	 * Check adjacency to other piece for a hanto coordinate
	 * 
	 * @return boolean true if adjecent to at least on game piece
	 */
	protected boolean checkAdjacency() 
	{
		int x = locationTo.getX();
		int y = locationTo.getY();
		
		return (board.containsKey(new HantoCoordinateImpl(x, y+1))
				|| board.containsKey(new HantoCoordinateImpl(x+1, y))
				|| board.containsKey(new HantoCoordinateImpl(x+1, y-1))
				|| board.containsKey(new HantoCoordinateImpl(x, y-1))
				|| board.containsKey(new HantoCoordinateImpl(x-1, y))
				|| board.containsKey(new HantoCoordinateImpl(x-1, y+1)));
	}
	
	
	/**
	 * Checks if piece is placed next to other piece from the same player
	 * 
	 * @return true if placement if valid
	 */
	protected boolean checkPlacementFriendly()
	{
		int x = locationTo.getX();
		int y = locationTo.getY();
		
		if(board.containsKey(new HantoCoordinateImpl(x, y+1))) {
			if(board.get(new HantoCoordinateImpl(x, y+1)).getColor() == onMove) {
				return true;
			}
		}
		if(board.containsKey(new HantoCoordinateImpl(x+1, y))) {
			if(board.get(new HantoCoordinateImpl(x+1, y)).getColor() == onMove) {
				return true;
			}
		}
		if(board.containsKey(new HantoCoordinateImpl(x+1, y-1))) {
			if(board.get(new HantoCoordinateImpl(x+1, y-1)).getColor() == onMove) {
				return true;
			}
		}
		if(board.containsKey(new HantoCoordinateImpl(x, y-1))) {
			if(board.get(new HantoCoordinateImpl(x, y-1)).getColor() == onMove) {
				return true;
			}
		}
		if(board.containsKey(new HantoCoordinateImpl(x-1, y))) {
			if(board.get(new HantoCoordinateImpl(x-1, y)).getColor() == onMove) {
				return true;
			}
		}
		if(board.containsKey(new HantoCoordinateImpl(x-1, y+1))) {
			if(board.get(new HantoCoordinateImpl(x-1, y+1)).getColor() == onMove) {
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Get all adjacent hexes for the given hex
	 * 
	 * @param hc HantoCoordinate to be checked
	 * @return List of HantoCoordinates
	 */
	protected List<HantoCoordinate> getAdjacentNeighbors(HantoCoordinate hc)
	{
		List<HantoCoordinate> neighbors = new ArrayList<HantoCoordinate>();
//		if(hc == null) {
//			return neighbors;
//		}
		int x = hc.getX();
		int y = hc.getY();
		if(board.containsKey(new HantoCoordinateImpl(x, y+1))) {
			neighbors.add(new HantoCoordinateImpl(x, y+1));
		}
		if(board.containsKey(new HantoCoordinateImpl(x+1, y))) {
			neighbors.add(new HantoCoordinateImpl(x+1, y));
		}
		if(board.containsKey(new HantoCoordinateImpl(x+1, y-1))) {
			neighbors.add(new HantoCoordinateImpl(x+1, y-1));
		}
		if(board.containsKey(new HantoCoordinateImpl(x, y-1))) {
			neighbors.add(new HantoCoordinateImpl(x, y-1));
		}
		if(board.containsKey(new HantoCoordinateImpl(x-1, y))) {
			neighbors.add(new HantoCoordinateImpl(x-1, y));
		}
		if(board.containsKey(new HantoCoordinateImpl(x-1, y+1))) {
			neighbors.add(new HantoCoordinateImpl(x-1, y+1));
		}
		
		return neighbors;
	}
	
	/**
	 * Get adjacent Hexes
	 * 
	 * @param hc HantoCoordinate
	 * @return List of Hanto Coordinates
	 */
	protected List<HantoCoordinate> getAdjacentHexes(HantoCoordinate hc)
	{
		List<HantoCoordinate> neighbors = new ArrayList<HantoCoordinate>();
		int x = hc.getX();
		int y = hc.getY();
		neighbors.add(new HantoCoordinateImpl(x, y+1));
		neighbors.add(new HantoCoordinateImpl(x+1, y));
		neighbors.add(new HantoCoordinateImpl(x+1, y-1));
		neighbors.add(new HantoCoordinateImpl(x, y-1));
		neighbors.add(new HantoCoordinateImpl(x-1, y));
		neighbors.add(new HantoCoordinateImpl(x-1, y+1));
		return neighbors;
	}
	
	/**
	 * Checks whether butterfly is placed by the end of the fourth turn
	 * @throws HantoException
	 */
	protected void checkButterflyByFourthTurn() throws HantoException
	{
		if(turn >= 7) {
			if(!currentPlayer.hasPlayedButterfly() && pieceOnMove != BUTTERFLY){
				throw new HantoException("current player did not place butterfly by fourth turn");
			}
		}
	}
	
	
	/**
	 * Record move into graph map
	 * @throws HantoException
	 */
	protected void recordMove() throws HantoException {
		if(pieceOnMove == BUTTERFLY) {
			currentPlayer.hasPlayedButterfly = true;
			if(onMove == BLUE){
				blueButterflyLocation = locationTo;
			} else {
				redButterflyLocation = locationTo;
			}
		}
		
		if(locationFrom == null) {
			currentPlayer.getPlayerPiece(pieceOnMove);
			currentPlayer.addPositions(locationTo);
		} else {
			board.remove(locationFrom);
			hexGrid.remove(locationFrom);
			currentPlayer.updatePositions(locationFrom, locationTo);
		}
		board.put(locationTo, makePiece(onMove, pieceOnMove));
		hexGrid.put(locationTo, (ArrayList<HantoCoordinate>) getAdjacentNeighbors(locationTo));
		updateHexGridLinks();
		updatePlayableHexes();
	}
	
	/**
	 * Update hex links 
	 */
	protected void updateHexGridLinks() 
	{
		for(HantoCoordinate hc: hexGrid.keySet()) {
			hexGrid.put(hc, (ArrayList<HantoCoordinate>) getAdjacentNeighbors(hc));
		}
	}


	
	/**
	 * Determines the state of the game
	 * @return MoveResult game state from move result
	 */
	protected MoveResult gameState()
	{
		MoveResult result = MoveResult.OK;
		if(redButterflyLocation != null && isSurrounded(redButterflyLocation)) {
			result = BLUE_WINS;
			gameOver = true;
		}
		if(blueButterflyLocation != null && isSurrounded(blueButterflyLocation)) {
			
			if(result == BLUE_WINS) {
				result = DRAW;
			} else {
				result = RED_WINS;
			}
			gameOver = true;
		} else if(turn == maxTurns) {
			result = DRAW;
			gameOver = true;
		}
		
		return result;
	}
	
	/**
	 * Checks if the given piece is completely surrounded by other occupied hexes
	 * @param at Hanto coordinate location of a piece
	 * @return true if hex is surrounded completely
	 */
	protected boolean isSurrounded(HantoCoordinate at)
	{
		int x = at.getX();
		int y = at.getY();
		return (board.containsKey(new HantoCoordinateImpl(x, y+1))
				&& board.containsKey(new HantoCoordinateImpl(x+1, y))
				&& board.containsKey(new HantoCoordinateImpl(x+1, y-1))
				&& board.containsKey(new HantoCoordinateImpl(x, y-1))
				&& board.containsKey(new HantoCoordinateImpl(x-1, y))
				&& board.containsKey(new HantoCoordinateImpl(x-1, y+1)));
	}
	
	
	/**
	 * Set from and to coordinate into HantoPieceImpl
	 * @param to location to
	 * @param from location from
	 */
	protected void setToFrom(HantoCoordinate to, HantoCoordinate from) {
		
		if(from != null) {
			locationFrom = new HantoCoordinateImpl(from);
		}
		if (to != null) {
			locationTo = new HantoCoordinateImpl(to);
		}

	
	}
	
	
	/**
	 * Checks if graph preserves the connectivity
	 * 
	 * @return true if graph still connected 
	 */
	protected boolean isConnected () 
	{

		Set<HantoCoordinate> set = new HashSet<HantoCoordinate>();
		Map<HantoCoordinate, ArrayList<HantoCoordinate>> temp = hexGrid;
		temp.remove(locationFrom);
		temp.put(locationTo, (ArrayList<HantoCoordinate>) getAdjacentNeighbors(locationTo));
		for(HantoCoordinate hc: temp.keySet()) {
			for(HantoCoordinate arrayHC: temp.get(hc)) {
				set.add(arrayHC);
			}
		}
		
		return set.size() == temp.size();
	}
	
	
	/**
	 * Validates that piece on the hex that it is move from
	 * 
	 * @throws HantoException
	 */
	protected void checkCorrectPiece () throws HantoException
	{
		if(locationFrom != null) {
			
			if (board.get(locationFrom).getType() != pieceOnMove) {
				throw new HantoException ("trying to move wrong piece type");
			}
			if (board.get(locationFrom).getColor() != onMove) {
				throw new HantoException ("trying to move wrong piece type");
			}
		}
	}
	
	/**
	 * Check if hex is empty that being moved
	 * 
	 * @throws HantoException
	 */
	protected void checkEmptyHex() throws HantoException
	{
		if(locationFrom != null) {
			if(board.get(locationFrom) == null) {
				throw new HantoException ("Trying to move empty hex");
			}
		}
	}
	
	/**
	 * Check if butterfly placed before sparrow can move
	 * 
	 * @throws HantoException
	 */
	protected void checkSparrowMoveBeforeButterfly () throws HantoException
	{
		if(onMove == BLUE) {
			if(blueButterflyLocation == null) {
				throw new HantoException("Blue can not move pieces before placing butterfly");
			}
		} else {
			if(blueButterflyLocation == null) {
				throw new HantoException("Red can not move pieces before placing butterfly");
			}
		}
	}
	
	/**
	 * Check if player made resignation
	 * 
	 * @param pieceType Hanto piece type
	 * @param from	location from
	 * @param to	location to
	 * @return boolean resignation is true
	 */
	protected boolean checkResignnation(HantoPieceType pieceType, HantoCoordinate from, HantoCoordinate to) {
		return pieceType == null && from == null && to == null;
	}
	
	/**
	 * If resigns is requested opponent player wins
	 * @return MoveResult for resignation
	 * @throws HantoPrematureResignationException
	 */
	protected MoveResult handleResignation() throws HantoPrematureResignationException
	{
		MoveResult result;
		if(onMove == BLUE) {
			result = RED_WINS;
		} else {
			result = BLUE_WINS;
		}
		gameOver = true;
		return result;
	}
	
	/**
	 * Get hanto game ID
	 * @return HantoGameID
	 */
	public  HantoGameID getHantoGameID()
	{
		return null;
	}
	
	/*
	 * @see hanto.common.HantoGame#getPieceAt(hanto.common.HantoCoordinate)
	 */
	@Override
	public HantoPiece getPieceAt(HantoCoordinate coordinate)
	{
		final HantoCoordinateImpl where = new HantoCoordinateImpl(coordinate);
		return board.get(where);
	}
	
	/**
	 * Updates possible hexes for move
	 */
	public void updatePlayableHexes()
	{
		List<HantoCoordinate> updates = getAdjacentHexes(locationTo);
		for(HantoCoordinate hc: updates)
		{
			playableHexes.add(hc);
		}
	}
	
	/**
	 * Get Hanto player
	 * @return hanto player
	 */
	public HantoPlayerColor getPlayer()
	{
		return onMove;
	}

	/*
	 * @see hanto.common.HantoGame#getPrintableBoard()
	 */
	@Override
	public String getPrintableBoard()
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Get map of the board
	 * @return map of hanto coordinates to pieces as a board
	 */
	public Map<HantoCoordinate, HantoPiece> getBoard() {
		return board;
	}
	
	
	
}
