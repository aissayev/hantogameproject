/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common.pieces;

import hanto.common.HantoPiece;
import hanto.common.HantoPieceType;
import hanto.common.HantoPlayerColor;

/**
 * Sparrow Class
 * @author aissayev
 *
 */
public class Horse implements HantoPiece{
	
	
	private final HantoPlayerColor mColor;
	
	/**
	 * Sparrow constructor
	 * @param color
	 */
	public Horse(HantoPlayerColor color)
	{
		mColor = color;
	}
	
	/**
	 * Get sparrow color
	 */
	public HantoPlayerColor getColor()
	{
		return mColor;
	}

	/**
	 * Get type Sparrow
	 */
	public HantoPieceType getType()
	{
		return HantoPieceType.HORSE;
	}

}