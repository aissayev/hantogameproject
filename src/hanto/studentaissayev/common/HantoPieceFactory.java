/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common;

import hanto.common.HantoPiece;
import hanto.common.HantoPieceType;
import hanto.common.HantoPlayerColor;
import hanto.studentaissayev.common.pieces.Butterfly;
import hanto.studentaissayev.common.pieces.Sparrow;
import hanto.studentaissayev.common.pieces.Crab;
import hanto.studentaissayev.common.pieces.Horse;

/**
 * Hanto Piece Factory class that makes a piece
 * @author aissayev
 *
 */
public class HantoPieceFactory {

	/**
	 * Makes given hanto piece 
	 * 
	 * @param color color of the piece to be made
	 * @param type	type of hanto piece to be made
	 * @return Hanto Piece
	 */
	public static HantoPiece makePiece(HantoPlayerColor color, HantoPieceType type)
	{
		HantoPiece piece = null;
		switch(type) {
			case BUTTERFLY:
				piece = new Butterfly(color);
				break;
			case SPARROW:
				piece = new Sparrow(color);
				break;
			case CRAB:
				piece = new Crab(color);
				break;
			case HORSE:
				piece = new Horse(color);
		}
		return piece;
	}
}
