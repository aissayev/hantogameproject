/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.common;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import hanto.common.HantoCoordinate;
import hanto.common.HantoException;
import hanto.common.HantoPiece;
import hanto.common.HantoPieceType;
import hanto.common.HantoPlayerColor;

import static hanto.common.HantoPieceType.*;

/**
 * Hanto player class defines the player in Hanto Game
 * @author aissayev
 *
 */
public class HantoPlayer 
{
	
	private final HantoPlayerColor mColor;
	private final Map<HantoPieceType, Integer> inventory;
	private final Set<HantoCoordinate> positions = new HashSet<>();
	protected boolean hasPlayedButterfly;
	
	/**
	 * constructor 
	 * @param color
	 */
	public HantoPlayer(HantoPlayerColor color)
	{
		mColor = color;
		inventory = new HashMap<HantoPieceType, Integer>();
		inventory.put(BUTTERFLY, 1);
		hasPlayedButterfly = false;
	}
	
	/**
	 * Set initial pieces for the player
	 * @param type	hanto piece type
	 * @param number number of piece to be set for a player
	 * @throws HantoException
	 */
	public void setPlayerPieces(HantoPieceType type, int number) throws HantoException
	{
		if(inventory.get(type) != null) 
		{
			throw new HantoException("Reset of " + type + " pieces");
		}
		inventory.put(type, number);
	}

	/**
	 * get give piece type
	 * @param type hanto piece type
	 * @return HantoPiece
	 * @throws HantoException
	 */
	public HantoPiece getPlayerPiece(HantoPieceType type) throws HantoException
	{
		if(inventory.get(type) == null) {
			throw new HantoException(type + " is not used in this version of the game");
		}
		int numPieces = inventory.get(type);
		if(numPieces <= 0) {
			throw new HantoException("You have no " + type + " pieces left");
		}
		inventory.put(type, --numPieces);
		if(type == BUTTERFLY) {
			hasPlayedButterfly = true;
		}
		return HantoPieceFactory.makePiece(mColor, type);
	}
	
	
	/**
	 * Check if player placed butterfly
	 * @return true if has butterfly
	 */
	public boolean hasPlayedButterfly() 
	{
		return hasPlayedButterfly;
	}
	
	/**
	 * Add position to the set
	 * @param hc hanto coordinate
	 */
	public void addPositions(HantoCoordinate hc)
	{
		positions.add(hc);
	}
	
	/**
	 * Update positions
	 * @param from location from
	 * @param to location to
	 */
	public void updatePositions(HantoCoordinate from, HantoCoordinate to)
	{
		positions.remove(from);
		positions.add(to);
	}
	
	
	/**
	 * If inventory is empty
	 * @return true if empty
	 */
	public boolean getInventorySize()
	{
		return inventory.isEmpty();
	}
	
	/**
	 * Return collection of available position
	 * @return
	 */
	public Collection<HantoCoordinate> getPositions()
	{
		return positions;
	}
	
}
