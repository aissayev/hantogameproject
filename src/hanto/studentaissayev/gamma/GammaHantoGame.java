/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2016 Adilet Issayev
 *******************************************************************************/
package hanto.studentaissayev.gamma;
import hanto.common.*;
import hanto.studentaissayev.common.*;


/**
 * Gamma variation of Hanto Game. Full game variation. All pieces and
 * all strategies are present in the game. Max turn is 40. Players can
 * not resign
 * @author aissayev
 *
 */
public class GammaHantoGame extends HantoGameBase {

	/**
	 * Constructor for Gamma Hanto Game
	 * @param movesFirst hanto player who makes first move
	 */
	public GammaHantoGame (HantoPlayerColor movesFirst)
	{	
		super(movesFirst);
		maxTurns = 40;
		bluePlayer = HantoPlayerFactory.makePlayer(HantoGameID.BETA_HANTO, HantoPlayerColor.BLUE);
		redPlayer = HantoPlayerFactory.makePlayer(HantoGameID.BETA_HANTO, HantoPlayerColor.RED);
		currentPlayer = movesFirst == HantoPlayerColor.BLUE ? bluePlayer : redPlayer;
		canResign = false;
		
	}

	
	
	

}
